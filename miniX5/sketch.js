
var imageList;
var imageList = [];
var imageRotate;
var button;


function preload() {
  img = loadImage("oliemaleri_baggrund.png");
  imgRamme = loadImage("knap_ramme.png");
  
  imageList = [];

  var imgCount = 9; //antal af billeder
  //loop der loader billederne ind i et array
  for (var i = 0; i < 9; i++) {
    imageList[i] = loadImage(i + ".png");

    
  }

  font = loadFont("VictorianText.ttf");
}

function setup() {
  // put setup code here
  createCanvas(windowWidth, windowHeight);
  angleMode(DEGREES);

  console.log(imageList);
  
  background(img);
  
  var n = 1000;

  for (var i = 0; i < n; i++) {
    var r = int(random(0, imageList.length));
    var randomImage = imageList[r]
    
    var x = int(random(-300, width ));
    var y = int(random(-300, height));

    imageRotate = random(0, 360);

    push();
    rotate(imageRotate);
    image(randomImage, x, y, randomImage.width, randomImage.height);
    pop();

  }
  

  image(imgRamme, 50, 50);
  button = createButton("Refresh");
  button.position(135, 250);
  button.size(80, 35);
  button.style("background-color", "#c5d0d0");
  button.style("font-size", "20px");
  button.style("font-family", "VictorianText")
  button.style("border-line", "ffffff");
  
  button.mousePressed(setup);


  textFont(font);
  textSize(22);
  textAlign(CENTER);
  text("Skab", 175, 160);
  text("nyt kunst", 175, 185);
  text("ved at trykke", 175, 210);
  text("på knappen", 175, 235);
}



function draw() {
  
}

//REFERENCELISTE

//Anvendte billeder:
//billedramme - https://www.freepik.com/free-vector/picture-frame-sticker-home-decor-vintage-gold-design-vector_20775420.htm#page=3&query=classical%20art&position=34&from_view=search&track=ais">Image by rawpixel.com</a> on Freepik
//Græsk krigerhovede - https://www.freepik.com/free-vector/head-with-helmet-vintage-illustration-remixed-from-artwork-by-john-flaxman_16327569.htm#page=2&query=classical%20art&position=14&from_view=search&track=ais">Image by rawpixel.com</a> on Freepik
//blad - https://www.freepik.com/free-vector/fragment-vector-illustration-remixed-from-artworks-by-sir-edward-coley-burne-ndash-jones_17204048.htm#query=classical%20art&position=18&from_view=search&track=ais">Image by rawpixel.com</a> on Freepik
//blomsteranordning - https://www.freepik.com/free-vector/blooming-iris-flower-vintage-vase_12459281.htm#page=4&query=classical%20art&position=17&from_view=search&track=ais">Image by rawpixel.com</a> on Freepik
//græsk mandehovede - https://www.freepik.com/free-vector/head-youth-statue-greek-god-aesthetic-post_15078477.htm#page=2&query=classical%20art&position=4&from_view=search&track=ais">Image by rawpixel.com</a> on Freepik
//damehovede - https://www.freepik.com/free-photo/greek-statue-engraving-style_17598786.htm#query=classical%20art&position=5&from_view=search&track=ais">Image by rawpixel.com</a> on Freepik
//nøgne damer - https://www.freepik.com/free-vector/three-graces-vector-nude-goddess-famous-painting-remixed-from-artworks-by-raphael_17204115.htm#query=classical%20art&position=13&from_view=search&track=ais">Image by rawpixel.com</a> on Freepik
//hænder - https://www.freepik.com/free-vector/finger-god-background-vector-aesthetic-brown-design_20346142.htm#query=classical%20art&position=25&from_view=search&track=ais">Image by rawpixel.com</a> on Freepik
//havfrue - https://www.freepik.com/free-vector/sea-nymph-vector-illustration-remixed-from-artworks-by-sir-edward-coley-burne-ndash-jones_17204055.htm#query=classical%20art&position=33&from_view=search&track=ais">Image by rawpixel.com</a> on Freepik

//baggrund:
//https://www.freepik.com/free-photo/abstract-oil-paint-textured-background_25267397.htm?query=classical art">Image by rawpixel.com</a> on Freepik

//font:
//https://www.1001fonts.com/victoriantext-font.html

//p5.js inspiration:
//https://editor.p5js.org/deibel.48/sketches/TknYctA3j