# MiniX8: Flowcharts 

## Individuel del

Den MiniX jeg har valgt at tegne et flowchart over er min miniX5. Til at tegne mit flowchart har jeg anvendt Figma. Forneden er der indsat billeder af mit flowchart men det er muligt og at se mit flowchart på Figma ved at følge linket:

- [Se flowchart i Figma](https://www.figma.com/file/WAqaI4Pt8igLnFDibk1Ftf/MiniX8?node-id=0%3A1&t=DQWK5MuYDOiIpTyr-1)

Mit flowchart er forholdvis stort at for at gøre det nemmere at overskue har jeg både indsat et billede over hele flowchartet men også opdelt det i mine to filer min MiniX5 består af.

![ALT](MiniX8_Flowchart.png) 

Det lilla flowchart viser mit idex.html fil. Det er forholdvis simplet og indeholder ikke særlig meget kode. Mit flowchart over mit sketch.js er det orange flowchart og indeholder en del flere steps. 

De ovale former i mit flowchart indikere start på selve mit program. En oval form kan også indikere et slutpunkt men jeg har valgt ikke at indsætte dette. Det skyldes at jeg ikke synes at mit program egentlig slutter. Der sker nødvendigvis ikke noget nyt medmindre man trykker på en knap men det slutter heller ikke. 

Rektangel formerne repræsentere steps i min proces. ALtid efter et step kommer der en eller flere parallelogrammer. De repræsentere enten input eller outputs. Jeg har forsøgt at forklare diverse input/outputs bedst muligt ved at skrive en lille kort beskrivelse. 

Alt er fobundet med pile der viser hvilken retning processen går. De stiplede linjer viser ikke en retning men er mere et forsøg på at indikere at der er en forbindelse mellem to elementer. Forseksempel hvis jeg tidligere har defineret en variabel viser den stiplede linje at den er forbundet/anvendes i et loop.

For ydereligere at skabe overblik over processen har jeg forsøgt at inddele de forskellgie trin i fire farver. De første trin er orange, der efter kommer gul, svag orange og til sidt svag gul. 

![ALT](MiniX8_Flowchat_index.htnl.png) 

![ALT](MiniX8_Flowchart_sketch.js.png) 

## Gruppe del
_Gruppe 10 - [Elma](https://gitlab.com/ElmaErkocevic/privat-aestetisk-programmering/-/tree/main/MiniX8) , [Laura](https://gitlab.com/laurakmj1512/aep/-/tree/main/Minix8) & Lisa_


### Flowchart 1: Flower data

Vores første flowchart omhandler vores idé, vi har valgt at kalde Flower Data. Hele ideen bygger på, at man kan få en blomst til at spire ved at man indtaster data, derimod kan man få blomsten til at visne, hvis man ikke vælger at indtaste data. 

Man begynder med at komme ind på vores hjemmeside/program, herinde ses der et billede af en blomst, og et tomt tekstfelt. I tekstfeltet kan der indtastes alle slags data. Hvis der i dette tekstfelt indtastes data indenfor 30 sekunder, bliver blomsten inde på hjemmesiden/programmet ved med at spire, og blomstens udseende ændres ved at billedet ændres. Man skal dog blive ved med at indsætte data indenfor 30 sekunder for at blomsten bliver ved med at spire. 

Hvis der ikke bliver ved med at indsættes data, så begynder blomsten lige så stille at visne, stadie for stadie, som alt sammen kan ses på billedet af blomsten. Blomsten kan dog altid reddes, hvis man vælger at begynde at indtaste data, hvor den så stadie for stadie igen begynder at spire.

![ALT](Gruppe_10_-_flowcharts__1_.png)

### Flowchart 2: Image Art

Vores anden flowchart omhandler ideen om en kunst generator, kaldet Image Art. Hele ideen bygger på at man tilgår vores hjemmeside/program hvor man ser en kunst generator. Når man kommer ind på siden, kan man vælge at indsætte et billede, så der kan udtrækkes RBG-data fra billedet. Hvis man ikke vælger at gøre det, kan der ikke udtrækkes RGB-data, og derfor kan kunstgeneratoren ikke genere kunst. 

Vælger man derimod at indsætte et billede, kan RGB-dataet udtrækkes. Efter RGB-dataet bliver udtrukket, generer kunstgeneratoren kunstværker vha. af dette. 

![ALT](Gruppe_10_-_flowcharts.png)

### Read Me

_What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?_

Det er altid svært at finde en måde, hvorved man kan kommunikere et budskab, så alle modtagere forstår det. I forbindelse med overvejelser vi har gjort os med kommunikationsniveauet og kompleksiteten af algoritmer kan vi se at der er en vigtig sammenhæng mellem de to ting. 

Algoritmer findes på mange forskellige niveauer og jo mere komplekse algoritmen er opsat, jo mere kompliceret kan det udbytte/budskab der kommunikeres ud være. De overvejelser vi har gjort os i forhold til udfordringer, er at det er vigtigt at vi i algoritmen simplificerer selve algoritmens output. At vi forsøger at koge vores outputs sammen i stedet for at vi har flere forskellige og ligegyldige outputs der muligvis ikke er nødvendige. 

<!-- blank line -->
----
<!-- blank line -->


_What are the technical challenges facing the two ideas and how are you going to address these?_

Vi forudser at de største tekniske udfordringer vi kommer til at opleve, er i forbindelse med data capture. Dette er gældende ved begge af vores ideer. 

Ved ideen Image Art tænker vi at vi muligvis får udfordringer ved at kode programmet, så det ikke kun indsamler et sæt RGB-data. Det vil sige at programmet ikke kun indhenter en farve. Vi ønsker at lave et stykke visuel kunst, der gør brug af flere forskellige farver. Derfor har vi brug for et stykke kode der indhenter flere farver. Derefter skal denne data som sagt også implementeres i den kode der genererer kunsten. Ved dette forestiller vi også visse udfordringer. Koden, der generer kunsten, skal også kunne modtage vores data capture og anvende det korrekt.

Flower data, som er vores anden ide, forudser vi visse problematikker i forhold til blomstens blomstrings niveauer. Som forklaret tidligere visner eller blomster blomstrer alt efter om der indtastes data. Hvordan vi skal opsætte disse niveauer, både i form af visning af blomsten og hvordan programmet reagerer på data inputtet/hvilken tilstand blomsten befinder sig i, har vi visse bekymringer om. 

Derudover er en af de vigtigste conditions i programmet Flower Data selve reglen om tid - altså hvis der ikke indtastes data inden for 30 sekunder visner blomsten. hvordan vi skal opstille netop denne condition og sørge for at det bliver eksekveret korrekt er vi på nuværende tidspunkt stadig i tvivl om. 

<!-- blank line -->
----
<!-- blank line -->

_In which ways are the individual and the group flowcharts you produced useful?_

De flowcharts vi har lavet i gruppen, håber vi senere hen kan bygge grundlag for vores afsluttende projekt, nemlig MiniX11. De to flowcharts formidler trin for trin hvordan vi ønsker at vores program skal fungere, og eksekveres, derfor er dette en hjælpende hånd når vi senere hen skal i gang med at kode vores idé.

Ligeledes, skal vi til vores afsluttende projekt lave en flowchart, der dykker mere ned i koden. Vi synes personligt, at det til tider kunne være svært at knække algoritmer, og koder ned i helt basale flowcharts, så det har helt klart været rart at kunne eksperimentere med det inden vores afsluttende projekt. Samt få noget erfaring i forhold til hvordan man bruger flowcharts i forbindelse med at visualisere koden/programmets proces.

