
// variabel for lyd
let mic;

//variabel for video og clmtracker
let capture;

//variabel farven i rect der bestemmer om lys er tændt/slukket
let rectColor;

//variabel for taster image
let img;
let img1;

//variabel for filterknapper
let button;




function preload() {
  img = loadImage("T_S_taster.png");
  img1 = loadImage("controlboard.png");
}



function setup() {
  // put setup code here
  createCanvas(displayWidth, displayHeight);
  
  //video
  capture = createCapture(VIDEO);
  capture.size(750, 350);
  capture.position(300, 300);
  capture.hide();
  
  //farve for lys tændt/slukket
  rectColor = color(0, 0, 0, 0);

  //lydinput
  mic = new p5.AudioIn();
  mic.start();


}



function keyPressed() {
  // keycode for S = sluk lyset
  if (keyCode === 83){
    rectColor = 10, 10, 10, 100;
    fill(250);
    textSize(30);
    textAlign(CENTER);
    
    text("Hvem overvåger dig og hvem overvåger du?", displayWidth/2, displayHeight*0.1);

    text("OBS!", displayWidth*0.86, displayHeight*0.76);
    textSize(16);
    text("Datatilsynet modtager hver måned et", displayWidth*0.86, displayHeight*0.8);
    text("sted mellem 600 og 800 anmeldelser ", displayWidth*0.86, displayHeight*0.84);
    text("af brud på persondatasikkerheden fra", displayWidth*0.86, displayHeight*0.88);
    text("virksomheder og offentlige myndigheder", displayWidth*0.86, displayHeight*0.92);

    text("For at slukke lyset tryk på S på dit tastertur", displayWidth*0.13, displayHeight*0.75);
    text("og for tænde det igen tryk på T.", displayWidth*0.13, displayHeight*0.79);
    image(img, displayWidth*0.13, displayHeight*0.85, 120, 50);

    image(img1, displayWidth*0.5, displayHeight*0.82, 700, 250);
   }
  // keycode for T = tænd lyset
   if (keyCode === 84){
    rectColor = "#71D0FA";
    textSize(30);
    textStyle("bold");
    fill(0);
    textAlign(CENTER);
    text("Hvem overvåger dig og hvem overvåger du?", displayWidth/2, displayHeight*0.1);
    fill(0);
    textSize(30);
    text("OBS!", displayWidth*0.86, displayHeight*0.76);
    textSize(16);
    text("Der er ca. 300.000 kamaraer placeret", displayWidth*0.86, displayHeight*0.8);
    text("i offentlige myndigheder, i offentlige ", displayWidth*0.86, displayHeight*0.84);
    text("transportmidler og kamaraer opsat af det ", displayWidth*0.86, displayHeight*0.88);
    text("danske politi i det offentlige rum.", displayWidth*0.86, displayHeight*0.92);
    
    image(img, displayWidth*0.13, displayHeight*0.85, 120, 50);
    text("For at slukke lyset tryk på S på dit tastertur", displayWidth*0.13, displayHeight*0.75);
    text("og for tænde det igen tryk på T.", displayWidth*0.13, displayHeight*0.79);

    image(img1, displayWidth*0.5, displayHeight*0.82, 700, 250);
   }
   
}



function draw() {
  // put drawing code here
  background(113, 208, 250);
  
  //Overskrift
  push();
  textSize(30);
  textStyle("bold");
  fill(0);
  textAlign(CENTER);
  text("Hvem overvåger dig og hvem overvåger du?", displayWidth/2, displayHeight*0.1);
  image(img1, displayWidth*0.5, displayHeight*0.82, 700, 250);
  pop()
  
;  //Infotekst
  push();
  textSize(15);
  textStyle("bold");
  fill(0);
  textAlign(CENTER);
  text("For at slukke lyset tryk på S på dit tastertur", displayWidth*0.13, displayHeight*0.75);
  text("og for tænde det igen tryk på T.", displayWidth*0.13, displayHeight*0.79);
  
  image(img, displayWidth*0.13, displayHeight*0.85, 120, 50);
  
  pop();

  //Lydinput
  let  volume = mic.getLevel();
  let mappedVolume = map(volume,0, 1, 0, 100);
  console.log(mappedVolume);
  
  //Mørklæggelse af skærmen
  push();
  fill(rectColor);
  rect(displayWidth/2, displayHeight/2, displayWidth, displayHeight);
  pop();
  
  
  

  //højtaler ---------------------------------
  push();
  fill(20);
  noStroke();
  rect(displayWidth*0.2, displayHeight*0.4, 150, 350, 30);
  rect(displayWidth*0.8, displayHeight*0.4, 150, 350, 30);
  pop();

  //højtaler - øverste speaker i venstre højtaler
  push();
  fill(90);
  noStroke();
  ellipse(displayWidth*0.2, displayHeight*0.3, 90+ mappedVolume);
  fill(50)
  ellipse(displayWidth*0.2, displayHeight*0.3, 75+ mappedVolume);
  fill(20);
  ellipse(displayWidth*0.2, displayHeight*0.3, 55+ mappedVolume);
  fill(50);
  ellipse(displayWidth*0.2, displayHeight*0.3, 10+ mappedVolume);
  pop();

  //højtaler - miderste speaker i venstre højtaler
  push();
  fill(90);
  noStroke();
  ellipse(displayWidth*0.2, displayHeight*0.4, 55+ mappedVolume);
  fill(50)
  ellipse(displayWidth*0.2, displayHeight*0.4, 40+ mappedVolume);
  fill(20);
  ellipse(displayWidth*0.2, displayHeight*0.4, 25+ mappedVolume);
  fill(50);
  ellipse(displayWidth*0.2, displayHeight*0.4, 5+ mappedVolume);
  pop();

  //højtaler - nederste speaker i venstre højtaler
  push();
  fill(90);
  noStroke();
  ellipse(displayWidth*0.2, displayHeight*0.5, 90+ mappedVolume);
  fill(50)
  ellipse(displayWidth*0.2, displayHeight*0.5, 75+ mappedVolume);
  fill(20);
  ellipse(displayWidth*0.2, displayHeight*0.5, 55+ mappedVolume);
  fill(50);
  ellipse(displayWidth*0.2, displayHeight*0.5, 10+ mappedVolume);
  pop();
  
  //højtaler - øverste speaker i højre højtaler
  push();
  fill(90);
  noStroke();
  ellipse(displayWidth*0.8, displayHeight*0.3, 90+ mappedVolume);
  fill(50)
  ellipse(displayWidth*0.8, displayHeight*0.3, 75+ mappedVolume);
  fill(20);
  ellipse(displayWidth*0.8, displayHeight*0.3, 55+ mappedVolume);
  fill(50);
  ellipse(displayWidth*0.8, displayHeight*0.3, 10+ mappedVolume);
  pop();

  //højtaler - miderste speaker i venstre højtaler
  push();
  fill(90);
  noStroke();
  ellipse(displayWidth*0.8, displayHeight*0.4, 55+ mappedVolume);
  fill(50)
  ellipse(displayWidth*0.8, displayHeight*0.4, 40+ mappedVolume);
  fill(20);
  ellipse(displayWidth*0.8, displayHeight*0.4, 25+ mappedVolume);
  fill(50);
  ellipse(displayWidth*0.8, displayHeight*0.4, 5+ mappedVolume);
  pop();

  //højtaler - nederste speaker i venstre højtaler
  push();
  fill(90);
  noStroke();
  ellipse(displayWidth*0.8, displayHeight*0.5, 90+ mappedVolume);
  fill(50)
  ellipse(displayWidth*0.8, displayHeight*0.5, 75+ mappedVolume);
  fill(20);
  ellipse(displayWidth*0.8, displayHeight*0.5, 55+ mappedVolume);
  fill(50);
  ellipse(displayWidth*0.8, displayHeight*0.5, 10+ mappedVolume);
  pop();

  //----------------------------------------------------------


  //tv
  push();
  rectMode(CENTER);
  noStroke();
  fill(90);
  rect(displayWidth/2, displayHeight*0.4, 700, 400, 5);
  imageMode(CENTER);
  image(capture, displayWidth/2, displayHeight*0.4, 640, 380);
  push();



  keyPressed()
  
}



