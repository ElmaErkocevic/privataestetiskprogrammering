# MiniX7: Genbesøg af miniX2


_Link til min [RunMe](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX7/index.html)_

_Link til min kode:_ 
- _[sketch.js](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX7/sketch.js)_
- _[style.css](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX7/style.css)_


Til trods for at jeg i min miniX6 udtrykte at jeg gerne ville forbedre den i denne miniX7 valgte jeg i stedet at gå med en af de tidligere miniX'er jeg har lavet. Det skyldes blandt andet at jeg havde set mig ret blindt (meget surt) på miniX6 og ville derfor gerne fokusere på en miniX som jeg ville forbedre uden at blive frustreret. Jeg valgte at fokusere på miniX2 da jeg ikke synes at jeg fik udført opgaven optimalt dengang og at jeg kunne have fokuseret mere på forbindelsen mellem emojis og deres kulturelle indflydelse/påvirkning for bruger.  

Min miniX2 var ret så simpelt og bestod af en simpel og neutral baggrund med en hvad jeg håbede, virkede som en neutral emoji i midten. Mit forsøg på at gøre emojien så neutral som muligt var at lave den som så standard som muligt. Blot en gul emoji der smiler. 

![ALT](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX7/miniX2_screenshot.png)

For at skabe til interaktion på siden forsøgte jeg desuden af få emojien til at vibrere. Desværre fik jeg ikke det til at lykkedes og som det kan ses via GIF'en forneden vokser den sig kun større. Ved at trykke på knappen vokser emojien og ved at slippe knappen igen stopper den. Dog stopper emojien med at ligne en emoji kort tid efter man har trykket på knappen.

![ALT](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX7/MiniX2_GIF.gif)

I forbindelse med denne miniX7 har jeg tænkt over at en standard emojien for mig for nogle kan virke som en repræsentation eller en ikke repræsentation. 
Denne tanke fik mig til at vende selve problematikken om: i stedet for at lave en emoji der passer til alle hvad så med en emoji brugeren kan tilpasse og derved altid føle sig repræsenteret og en form for personlig tilknytning til emojien. 

<!-- blank line -->
----
<!-- blank line -->

For mig er det vigtigt at det visuelle spiller sammen. Og jeg synes ikke det gjordet ved min miniX2. Til trods for mit forsøg på at skabe en neutral setting føles den mere dyster og kedelig. I denne miniX7 forsøger jeg at opstille mit canvas således at det ligner at man faktisk kigger på en emoji på en telefon og at man selv er i gang med at personligere den. Til højre har jeg placeret en boks hvor man kan foretage ændringer af en emoji i form at hudfarven og dens øjenfarve. Jeg er bevidst om at der findes andre træk ved personer repræsenterer dem men i dette tilfælde forsøger jeg at holde det simpelt. Forneden kan der via billedet ses at jeg gør brug af sliders til at ændre emojiens udseende.

![ALT](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX7/screenshot_start.png)

Ved både hudfarve og øjenfarvesektionen valgte jeg at bruge 3 sliders ved. Jeg bruger `RGB` ved sliderne og hver slider har hver sin parameter. Det vil sige at den første slider styrer farve `rød`, slider to styre `grøn` og slider tre styre `blå`. Da jeg ønskede i alt seks sliders og jeg derfor havde behov for at adskille `RGB` parametrene, det vil sige at når jeg ændrede `rød` ved hudfarve så ændrede den sig ikke ved øjenfarven. I min kode definerede værdierne for farve parametrene. `var r`, `var g` og `var b` er farveværdierne for hudfarve slidernes og `var rr`, `var gg` og `var bb` er for øjenfarvesliderne. 

![ALT](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX7/screenshot_kode1.png)

![ALT](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX7/screenshot_kode3.png)



Til trods for at jeg i min miniX2 ville skabe en form for neutralitet og mindske de kulturelle repræsentationer i min emoji vil jeg i dette tilfælde meget gøre dem tydeligere og mere mulige. Derfor valgte jeg at star værdierne for emojien netop er mulat. I billedet forneden kan der ses at jeg i linje 54-56 definere startværdierne for `RGB`. Derefter definere jeg hvor hver slider skal placeres samt deres style i form af `width`. 


![ALT](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX7/screenshot_kode2.png)

![ALT](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX7/emojiex.png)


Som tidligere nævnt forsøgte jeg i min miniX2 at få emojien til at vibrere men i stedet voksede den. Kort tid efter miniX2 fik jeg hjælp til at få emojien til at vibrere og ønskede ikke at fjerne det fra koden. I min `function draw()` definere jeg at hvis knappen er `pressed` skal emojiens position vælges random.

![ALT](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX7/screenshot_kode4.png)

![](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX7/miniX7_video.mov)

<!-- blank line -->
----
<!-- blank line -->

Ligesom med miniX2 gjorde jeg igen nogle overvejelser i forhold kulturelle, identitet og race repræsentation i forbindelse med emojis. Jeg forsøgte med denne forbedring at gøre det mere muligt at flere kunne fæle sig repræsenteret. Jeg kan forstå at vi lever en tid hvor mange føler et behov for at de kan føle sig inkluderet i ting som de ikke før har kunne føle sig en del af. Og jeg indrømmer at jeg da selv følte en vis glæde da jeg så at man kunne få emojis der er mulat. Men jeg tror stadig det er svært i netop dette tilfælde med emojis. Der vil altid være nogle der føler sig ekskluderet eller ikke repræsenteret. 

I stedet for at skabe flere grupper så i stedet skabe en fælles gruppe hvor alle netop kan føle sig inkluderet uanset hvilken etnicitet, politisk holdning, kultur og tro man skulle have. Dette er selvfølgelig sikkert umuligt da det ligger i vores natur at danne grupper. Men man har vel lov at drømme.


<!-- blank line -->
----
<!-- blank line -->

## Referencer

- p5.js Referencer - https://p5js.org/reference/


**Anvendte billeder:**
- Baggrund: https://www.freepik.com/free-vector/town-street-with-buildings-trees-empty-pavement_3297824.htm#&position=1&from_view=undefined
- Mobil: https://www.freepik.com/free-vector/hand-holding-smartphone-mobile-phone-concept-hand-drawn-cartoon-art-illustration_22179706.htm#&position=3&from_view=undefined

