# Gruppe 10
Link til vores individuelle Gitlabs:
- [Elma](https://gitlab.com/ElmaErkocevic/privat-aestetisk-programmering/-/tree/main/MiniX8)
- [Laura](https://gitlab.com/laurakmj1512/aep/-/tree/main/Minix8)
- [Lisa](https://gitlab.com/lisabirungi/privataestetiskprogrammering/-/tree/main/miniX8)


# Feedbackskema
Link til cryptopad med oversigt over feeedback: [Cryptopad](https://cryptpad.fr/sheet/#/3/sheet/edit/6f94b81fc3bec21d06b39042e9cb8c8b/)

| MiniX8 |  MiniX9 | MiniX10 | MiniX11 |
|:----------------|:------------------|:---------------|:---------------|
| Ingen feedback | Skrift. feedback til gr. 1 | Ingen feedback | Mundtlig feedback til gr. 3|


<!-- blank line -->
----
<!-- blank line -->

# MiniX oversigt

🔴 **_TEKST OG MINIX OVERSIGT ER FRA [MARGRETHES GITLAB](https://gitlab.com/Margretelr/aestetisk-programmering-2023/-/blob/main/minix/README.md) (OBS TJEK FOR OPD!)_** 🔴

### MiniX[8]: Flowcharts (individuel + gruppe)
💻 **Øvelse**: Aflevering søndag d.16 april inden midnat<br>

Din egen repository skal både pege på din individuelle flowchart og gruppens.

[Øvelsen står beskrevet i grundbogen kap. 9](https://aesthetic-programming.net/pages/9-algorithmic-procedures.html#minix-flowcharts-41979:~:text=operations%20of%C2%A0programming.-,MiniX,-%3A%C2%A0Flowcharts)


(Omkring 3500-5000 anslag)

✍ **Feedback**: INGEN FEEDBACK på denne miniX

<br>


### MiniX[9]: E-lit (gruppe)
💻 **Øvelse**: Aflevering søndag d.23 april inden midnat<br>

[Øvelsen står beskrevet i grundbogen kap. 7](https://aesthetic-programming.net/pages/7-vocable-code.html#required-reading-94018:~:text=making%20binaries%C2%A0strange.-,MiniX,-%3A%C2%A0E%2Dlit)


(Omkring 3500-5000 anslag)

✍ **Feedback**: Aflevering onsdag d.26 april inden undervisning <br> 

- Redegør for
    - Hvad er programmet og hvordan virker det?
    - Er der nogle nye syntakser, som du ikke kendte til?
    - Hvad gør det til en e-lit (electronic literature)?
    - Er der en explicit kobling til litteraturen eller koncepter fra ÆP/SS?

- Reflektere over
    - Kan du lide designet? Hvorfor/hvorfor ikke?
    - Hvordan er brugen af gruppe strukture og JSON anderledes eller ens med din egen miniX?
    - Hvordan vil du analysere værket ift. perspektivet om kode og sprog, som åbner op for æstetisk refleksion?

<br>

### MiniX[10]: Machine unlearning (gruppe)
💻 **Øvelse**: Aflevering søndag d.3 maj inden midnat<br>

**Formål**:
- Selvstændigt at udforske og tinker med machine learning biblioteker og funktioner 
- At stille spørgsmål ved og forstå de eksisterende syntakser/logikker/programmeringsstrukture/funktioner, som relatere sig til machine learning

**Opgave (RunMe og ReadMe)**:

Denne gang behøver I ikke at designet et nyt koncept/værk/artefakt. I skal derimod udvælge et eksempel kode fra ml5.js hjemmesiden (fx. ImageClassifier, PoseNet, StyleTransfer, CharRNN osv.), som I forsøger at forstå ved at køre den og ændre variablerne lokalt på jeres computer.

Hvis der opstår nye parameter, som I ikke kender til eller forstår, så undersøg på nettet og find en måde at forstå de specifikke teknikaliteter ved machine learning (fx. temperatur i CharRNN, klassifikationer i ImgaeClassifier, RNN osv.).

Målet med denne miniX er at forstå, undersøge og diskuttere machine learning ved at engagere sig i de computationelle materialer, formulere forspørgsler til søgemaskinen, og at læse/køre kode.

Vis os et screenshot af jeres test og/eller jeres eksperimenterende sketch.

**ReadMe**:
- Hvilken eksempel kode har I valgt, og hvorfor?
- Har I ændret noget i koden for at forstå programmet? Hvilke?
- Hvilke linjer af kode har været særlig interessante for gruppen? Hvorfor?
- Hvordan ville I udforske/udnytte begrænsningerne af eksempelkoden eller machine learning algoritmer?
- Var der syntakser/funktioner, som I ikke kendte til før? Hvilke? Og hvad lærte I?
- Hvordan kan I se en størrer relation mellem eksempel koden, som I har undersøgt, og brugen af machine learning ude i verdenen (fx. kreative AI, Voice Assistance, selvkørende biler, bots, ansigtsgenkendelse osv.)?
- Hvad kunne du tænke dig at vide mere om? Eller hvilke spørgsmål kan du ellers formulere ift. emnet?

(Omkring 3500-5000 anslag)

✍ **Feedback**: INGEN FEEDBACK på denne miniX, men hver gruppe skal præsentere miniX[10] om onsdagen <br> 

<br>


### MiniX[11]: Endelige projekt (gruppe) + draft
📝**Draft**: Aflevering søndag d.7 maj inden midnat

Lav et draft om jeres projekt på 3-4 sider (eksklusiv billeder, referencer og noter) i PDF format, og upload det til BrightSpace:

(Gå til BrightSpace > Course tools > Discussion > Endelige projekt draft aflevering > din gruppe > Start a new Thread)

PDF'en skal indeholder:
- En referenceliste
- Relevante sketches og billeder
- En titel på projektet og en revideret flowchart

✍ **Feedback**: Afgiv mundtlig feedback onsdag d.10 maj til vejledningsgangen <br> 

Forberede mundtlig feedback på 8 minutter om følgende emner:
- Brugen af litteratur og koncepter
- Kobling til temaerne i ÆP og kusets litteratur
- Tydeligheden af projektet (sproglig formidling)
- Hvad kan du bedst lide, og hvorfor?
- Hvilke elementer er mindre succesfulde? Hvilke forbedringsforslag har du?
- Øvrige kommentare


💻 **Øvelse**: Det endelige projekt skal afleveres onsdag d.17 maj<br>

[Øvelsen står beskrevet i grundbogen kap. 10](https://aesthetic-programming.net/pages/10-machine-unlearning.html#minix-final-project-24294:~:text=as%20a%C2%A0consequence%3F-,MiniX,-%3A%20final%C2%A0project)

Det er dette endelige projekt, som I vil blive spurgt ind til ved eksamen. MiniX[11] fungerer altså lidt som jeres synopsis.


