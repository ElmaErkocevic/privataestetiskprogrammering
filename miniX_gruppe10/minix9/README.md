# MiniX9: E-lit

_Link til vores [RunMe](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX_gruppe10/minix9/index.html)_

_Link til vores kode:_ 
- _[index.html](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX_gruppe10/minix9/index.html)_
- _[sketch.js](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX_gruppe10/minix9/sketch.js)_
- _[jason.json](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX_gruppe10/minix9/jason.json)_

# Provide a title of your work and a short description (1,000 characters or less).

###### Værkets navn er "Trykker"

Vi trykker for ændring, fra dem der kan styre, vi trykker dem der lokalt køber, vi trykker programmeringen ind, vi trykker formidlingen.
Når du trykker, skal du lytte. Værket er en rykker på en ændring, det skal ikke være rart, det skal føles som en bøde. Som noget der trykker, både indefra og udefra. Behovet for tilpasning udadtil, trykker miljøet. Sætter naturen under pres, for at leve op til en umulig kapitalistisk standard. Virksomheder gemmer sig under overfladiske og falske lovninger om grøn omstilling i deres produktion. Vi bliver narret af grøn indpakning og "40% genbrugt polyester". Men når jorden er fuld, er det ikke genbrugt polyester eller pap indpakninger med plastik solbriller i, der rykker. Verden er fuld og nu skal du vælge hvem du trykker.


# Describe how your program works, and what syntax you have used, and learnt?

Programmet er en hjemmeside der med stor samlignelighed ligner Zalando. Hjemmesiden viser nogle billeder af nogle simple styles. Når du trykker på hjemmesiden, kommer der fakta frem om overproduktion i tøjindustrien. Dette gøres i en blodig typewriter style skift.

![](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX_gruppe10/minix9/Trykker.mov)

Koden vi har anvendt til vores værk ”Trykker” er ikke særlig kompliceret.  Simpliciteten i det og det tunge i den information som vi via vores kode gerne vil vise, skaber en stærk kontrast mellem hinanden. I denne opgave er fokus at gøre brug af JSON filer, som er en tekstbaseret fil hvor i vi opbevare data. Forneden kan der ses et billede hvor vi definere vores globale værdier. I linje 1 definere vi variablen `myJson` som vi anvender til at hente vores JSON-fil i vores `function preload()`. Dette kan ses i vores linje 18.

![ALT](kode_1.png)

Vores JSON-fil er som sagt tekstbaseret og består af den information vi ønsker at visualisere i vores værk. I billedet forneden kan vores JSON-fil ses. Når vores programmet går i gang og vores JSON-fil loades, trækkes der data fra vores ”voresSkyld” array, hvor vi i vores kode har valgt at der random skal udvælges blandt vores ”sandheden” og ”paavirkning” værdier.  Selve tankerne bag vores navngivning af vores forskellige værdier, variabler og array er ikke tilfældet. 

Ligesom med ”Vocable Code” eksemplet, forsøger vi ikke kun via programmets visuelle output, at skabe fokus på modeindustriens negative påvirkning og os som forbrugers overforbrug. Hvert fakta i vores array hedder `sandheden` fordi det netop er sandheden bag modeindustrien vi gerne vil belyse. Selve værdien der vises, hedder `paarvirkning` grundet det er de negative påvirkninger som er essensen i det der visuelt skal vises i vores værk. Da disse to værdier befinder sig i vores array, har vi valgt at navngive arrayet `voresSkyld`. Dette er en metafor på at hver t-shirt vi producere, køber og smider ud og de effekter der er bag hver af disse handlinger – både negative og positive – er vores skyld.

![ALT](kode_4.png)

I vores `function draw()` er hvor vi trækker vores data fra vores JSON-fil. I billedet forneden kan dette samt stylingen af den tekst vores program viser ses. Vi har valgt at style vores `text` i rødlige nuancer for at skabe en dramatisk og mindeværdigt indtryk. De røde nuancer vælges random hvilket også er gældende for teksten størrelsen.

![ALT](kode_2.png)

For at få teksten vist har vi gjort brug af `function mouseClicked()`. Da vi kun ønsker at få hele tal fra vores array, gør vi brug af funktionen `floor`. Vi definerer `i` til at det skal være random mellem 0 og 8 da vi har 9 sætninger i vores array. Vi har allerede globalt defineret vores `i` værdien til at være 0.  Derefter definere vi at vores tekst skal vises. I vores `text` ønsker vi, at det der skal trækkes data fra vores JSON-fil og vi ønsker at det er fra vores array `voresSkyld` hvor der random skal udvælges en `paavirkning`. Placeringen af vores `text` er derudover sat til random netop for at skabe en overraskelses effekt når teksten vises.

![ALT](kode_3.png)


# How would you reflect on your work in terms of Vocable Code? Analyze your own e-lit work by using the text Vocable Code and/or The Aesthetics of Generative Code (or other texts that address code/voice/language).

Hjemmesider-som Zalando- er designet med alle begreberne fra Affordance bøgerne. De fortæller os præcis hvad de vil have ud af os uden at skrive det. Det kan pirre os, vi får lyst til trykke, vi får behov for ting og tøj vi ikke havde før. Når marketing møder programmering og digitalt design går det op i en højere enhed af euforisk underbevidst-manipuleret overforbrug. 
Vores trick hjemmeside her har til formål at provokere, irritere og skuffe forbrugeren. Alle de programmerede salgstricks er ufunktionelle, de kan hverken købe, komme videre eller få overblik.
I sin grundvold spiller hjemmesiden på principperne fra en klassisk fælde, blot med symbolske eftervirkninger, frem for den klassiske fysiske eller økonomiske straf. Man kan egentlig sige at Zalando fældens straf er dårlig samvittighed, fordi man netop bliver taget med fingrene i kagedåsen.

Miljø og overforbrug er et svært emne, fordi det for de fleste virker fjernt eller mindre vigtigt end egen trivsel. Derudover er miljøet billedligt svært at gøre indtryk med. Flasker i havet, døde måger, træer der dør og is der smelter er langt væk fra Danmark. Og de fleste har det indtryk at "en t-shirt skader ikke" eller "alle andre gør der, så hvorfor skal jeg nøjes" Den moderne miljødebat clasher også lidt med den moderne opfattelse af det gode liv, med stor material gevinst. Derfor har vi valgt at være mere konkrete og gøre det meget bogstaveligt. Lidt ligesom kodning er.
Den generative kode gør hjemmesiden uforudsigelig, utryg og frastødende. Den er tiltrækker forbugeren og skubber den væk med det samme. Benytter fjendens tricks så at sige. Den røde tekst sætter os på kanten, gør os opmærksom på fatale konsekvenser. Hvorimod de hårde facts servere det for os.

