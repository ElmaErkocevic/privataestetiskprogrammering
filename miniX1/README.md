# MiniX1: RunMe og ReadMe

_Link til [MiniX1](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX1/index.html)_

For at få en grundlæggende forståelse af sproget p5.js, har jeg valgt at finde inspiration i de mere enkle syntakser, som kan findes på referencesiden på [p5js.org](https://p5js.org). 

De syntakser som jeg har fundet inspiration fra, er blandt andet ```mouseClicked()``` og ```fill()```. Det jeg har kodet, er en simpel side der består af en simpel baggrund hvor der er placeret ni elementer. Elementerne består af tre forskellige former og hvert element har hver sin farve. 

Elementerne er opstillet i en kvadrant på tre gange tre elementer. Hver kolonne indeholder hver sin form. I den første er der placeret 3 cirkler, i anden kolonne 3 firkanter og i den sidste kolonne tre trekanter. Når brugeren af siden klikker hvilket som helst sted på siden, skifter baggrunden til en ny tilfældig farve. Dog starter siden altid med den samme baggrundsfarve når siden refreshes. 

Forneden kan der ses en GIF, der viser hvordan figurerne er opstillet samt hvordan baggrunden skifter farve hver gang der trykkes et sted på siden.

<br>

![ALT](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX1/GIF_eksempel.gif)

<!-- blank line -->
----
<!-- blank line -->

Fra en tidligere uddannelse har jeg i forvejen en viden om kodning. Dog ikke om p5.js. Denne viden er ved at være nogle år gammel og derudover har jeg ikke vedligeholdt min viden om emnet de sidste par år. Dengang var det, og er på nuværende tidspunkt, mest CMS (Content Management Systemer) som jeg gør brug af, i forbindelse med opsætning og vedligeholdelse af websider.

Jeg har altid fundet selve designdelen og de visuelle forberedelser inden kodning mest interessant. Blandt andet hvordan en webside skal se ud, hvad den skal kunne, hvilke designmæssige udfordringer der kan være og hvilke behov der kan være.

Jeg fandt det dog stadig sjovt og tilfredsstillende, at udføre denne opgave. Som jeg husker og hvad jeg opdagede under denne opgave, finder jeg det stadig underholdende og tilfredsstillende, at observere syntakser bestående af tal og kommandoer udforme sig til noget visuelt i sidste ende. At noget man har lavet fungere og at det ikke kun kan ses og bruges af mig men også andre opleve det finder jeg personligt belønnende. 

<!-- blank line -->
----
<!-- blank line -->

Det er særdeles hjælpsomt, at der både på [p5js.org](https://p5js.org) findes en reference side men også, at der andre steder online findes en del hjælpemidler. Det at man kan finde eksisterende kode, finde inspiration fra det og derved tilpasse det til ens personlige behov, gør der nemmere at lære sproget og hvordan man anvender det.

Det som tidligere og som stadig bekymre mig er, hvor går grænsen for hvad der ens eget tilpasset kode og hvad der anses som blot kopieret. Jeg kender nu til de basale syntakser og kan derved blot indskrive det i Visual Studio Code som derved også vil komme med forslag. 

Men at skulle finde på ny kode fra bunden, tror jeg ikke jeg vil være i stand til. Hvor går grænsen i forhold til hvornår noget er en kopiering og hvornår har man tilpasset kode nok til, at man kan kalde det sin egen kode er for mig stadig en smule uklar og usikker. 

<!-- blank line -->
----
<!-- blank line -->

Jeg anser kodning som at være særdeles anderledes end blot at skulle skrive en normal tekst eller læse en normal tekst. I kodning opfatter jeg, at man gerne skal være direkte og der er ikke plads til fejl. Hvis man laver en fejl i sin kode såsom man mangler et komma, et bogstav eller man indtaster en ikkeeksisterende syntaks, kan man risikere at intet fungerer. Derefter finder jeg det nogle gange svært at observere fejlen. Det kan både være fordi jeg ikke forstår hvor fejlen skulle være eller fordi jeg har set mig blind i forhold til et manglende komma. 

Ved en normal tekst man for eksempel skriver i et Word dokument, er indholdet ikke nødvendigvis afhængig af at alt hænger sammen. At den overordnede tekst hænger sammen overordnede er selvfølgelig en god ting, men hvis et afsnit eller blot en enkel sætning ikke er korrekt, er det stadig muligt at hele teksten kan fungere som en helhed. Sproget man skriver, er selvfølgelig også anderledes. 

<!-- blank line -->
----
<!-- blank line -->

Jeg glæder mig både til at lære mere omkring emnet, men samtidig er jeg også urolighed. Jeg elsker at være kreativ og visuelt vil jeg meget gerne ofte skabe noget lækkert, specielt eller noget unikt. Derfor kunne jeg godt forestille mig, at jeg muligvis ville få en tendens til at bide over mere end jeg kan klare. 

Men som sagt glæder jeg mig også, da netop en baggrundsviden og en forståelse af kodning, uanset om man er god til emnet eller ej, er en fordel i forhold til det vi tidligere har lært og som vi sikkert også kommer til at lære senere hen i uddannelsen. Til trods for at denne opgave ikke indeholder en svær kode og ikke har de største krav, har det givet mig en positiv indstilling samt behagelig start på dette kursus. 

<!-- blank line -->
----
<!-- blank line -->

### Referenceliste
- [p5.js.org - ellipse( )](https://p5js.org/reference/#/p5/ellipse)
- [p5.js.org - square( )](https://p5js.org/reference/#/p5/square)
- [p5.js.org - triangle( )](https://p5js.org/reference/#/p5/triangle)
- [p5.js.org - mouseClicked( )](https://p5js.org/reference/#/p5.Element/mouseClicked)
- [p5.js.org - color( )](https://p5js.org/reference/#/p5/color)
- [gitlab.com - README guide](https://about.gitlab.com/handbook/markdown-guide/)
- [github.com - README guide](https://docs.github.com/en)


